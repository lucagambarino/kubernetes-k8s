<!-- Autore = Luca Gambarino -->

![Build Status](https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Kubernetes_logo_without_workmark.svg/247px-Kubernetes_logo_without_workmark.svg.png) $`\textcolor{blue}{\text{KUBERNETES}} `$ 

---

## REQUISITI:
- N° minimo di 2 Nodi (Master e Worker)
- Ciascuno composto da 2 Processori + 4GB di RAM + 20GB di HD + 1 Sk di rete (nel caso di VM abilitare il bridging)  
- UBUNTU Serevr 20.04.4 LTS
- kubelet kubeadm kubectl - tutti alla versione 1.24.0-00
- CRI opzione CONTAINERD oppure opzione CRI-O dopo che è stato reso deprecato il sistema docker engine
- Network CALICO e NGINX Ingress

```mermaid
graph LR;
  A[Server Master 192.168.1.201]-->|Cluster K8s <br> 10.0.0.0/16|B[Server Worker 192.168.1.202];
```
---

**Table of Contents** 
- [Installazione](#installazione)
- [Opzione CONTAINERD](#opzione-containerd)
- [Opzione CRI-O](#opzione-cri-o)
- [Creazione Cluster KUBERNETES](#creazione-cluster-kubernetes)
    - [Sul nodo MASTER](#sul-nodo-master)
    - [Sul nodo WORKER](#sul-nodo-worker)
    - [Installazione di CALICO sul MASTER](#installazione-di-calico-sul-master)
    - [Installazione NGINX INGRESS sul MASTER](#installazione-nginx-ingress-sul-master)
- [Test di Deployment pod](#test-di-deployment-pod)
- [Troubleshooting](#troubleshooting)
- [Riferimenti](#riferimenti)

## Da fare su tutti i nodi

## Installazione
- Installare sui 2 server il S.O. Ubuntu 20.04.4 in versione minimale (installando solo OpenSSH senza altri pacchetti)
- Language: English
- Versione Installer su "Continue without updating"
- Keyboard Layout: Italian
- Keyboard Variant: Italian
- Sk rete in manual con assegnati gli IP 192.168.1.201 e 192.168.1.202 uno per ciascun server
- Se a disposizione c'è un proxy inserite i parametri, altrimenti andate avanti
- Salezionate archive mirror che vi interessa, altrimenti lasciate il default
- Per lo storage lasciate intero disco da 20GB e la selezione a "Set up this disk as an LVM group"
- Cliccare su done e su Continue
- Inserire il vostro nome, il nome del server, l'utente amministrativo e la password confermandola
- Selezionare l'opzione "Install OpenSSH server"
- Non selezionare alcun pacchetto aggiuntivo
- Lasciare finire l'installazione e alla fine riavviare il sistema

Aggiornamento del sistema operativo:
```
sudo apt-get update && sudo apt upgrade -y
```
```
sudo reboot
```

Sul sistema è presente NetworkManager per cui bisogna disabilitare le interfacce per poter utilizzare CALICO:
```
sudo mkdir -p /etc/NetworkManager/conf.d

sudo touch /etc/NetworkManager/conf.d/calico.conf

sudo cat <<EOF | sudo tee /etc/NetworkManager/conf.d/calico.conf
[keyfile]
unmanaged-devices=interface-name:cali*;interface-name:tunl*;interface-name:vxlan.calico;interface-name:wireguard.cali
EOF
```

Sul sistema modificare i moduli del kernel
```
sudo cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

# sysctl params required by setup, params persist across reboots
sudo cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

# Apply sysctl params without reboot
sudo sysctl --system
```

Disabilitazione dello SWAP:
```
sudo swapoff -a
```

Commento in fstab per lo SWAP. Tale operazione è necessaria al riavvio perchè non venga avviato lo SWAP:
```
sudo vi /etc/fstab

# /swap.img      none    swap    sw      0       0
```

Aggiornamento del sistema:
```
sudo apt-get update
```

Installazione del pacchetto dei certificati ed aggiunta del repository:
```
sudo apt-get install -y apt-transport-https ca-certificates curl
```
```
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
```
```
sudo echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
```

Aggiornamento del sistema dei repository:
```
sudo apt-get update
```
```
sudo apt-get install -y kubelet=1.24.0-00 kubeadm=1.24.0-00 kubectl=1.24.0-00
```
```
sudo apt-mark hold kubelet kubeadm kubectl
```

Riavviare i server:
```
sudo reboot
```
## Opzione CONTAINERD
Installazione del pacchetto dei certificati ed aggiunta del repository:
```
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```
```
sudo echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Aggiornamento del sistema dei repository:
```
sudo apt-get update
```

Installazione containerd.io ed abilitazione di default sulla maccchina:
```
sudo apt-get install containerd.io
```
```
sudo systemctl enable containerd.service
```

Commento alla disabilitazione del plugin CRI:
```
sudo vi /etc/containerd/config.toml

# disabled_plugins = ["cri"]
```

Riavvio servizio containerd:
```
sudo systemctl restart containerd

sudo ls -l /run/containerd/containerd.sock
    Risposta  "srw-rw---- 1 root root 0 May 14 15:51 /run/containerd/containerd.sock"
```

## Opzione CRI-O
*** Requisito: libseccomp >= 2.4.1 e impersonare l'utente root ***

```
sudo su -
```

Installazione del pacchetto:
```
apt-get install -y libseccomp2
```

Preparazione ed installazione di CRI-O:
```
OS=xUbuntu_20.04
VERSION=1.22

echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
echo "deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/$VERSION/$OS/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable:cri-o:$VERSION.list

curl -L https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$VERSION/$OS/Release.key | sudo apt-key add -
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/Release.key | sudo apt-key add -

apt update
apt install cri-o cri-o-runc
```
```
apt-cache policy cri-o
```
```
systemctl daemon-reload
systemctl enable crio --now
```
```
systemctl status crio
```
```
apt install cri-tools
```
```
crictl info
```

X TEST dare il comando:
```
crictl pull nginx:latest
```

Verificare l'immagine scaricata:
```
crictl images
```

*** Tornare all'utenza normale amministrativa non root ***
```
logout
```

## Creazione Cluster KUBERNETES

## Sul nodo MASTER
```
sudo kubeadm config images pull
```
*** Attendere un pò di tempo per lo scaricamento delle immagini di K8s. Il tempo dipende dalla vostra connessione a internet ***

Avvio cluster K8s:
```
sudo kubeadm init --pod-network-cidr=10.0.0.0/16
```

Dovrebbe apparire un messaggio simile al seguente:
```
To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 192.168.1.201:6443 --token 7qshlv.rscmjw3fjki5pq2a \
        --discovery-token-ca-cert-hash sha256:3e3b9644ac29f53af2e3395bd200ce98f0610b74f063a35d5a13909171e779a7

```

Impartire i comandi:
```
mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

## Sul nodo WORKER
*** Ricordarsi di cambiare il token secondo quello scritto nel messaggio del master ***
```
sudo kubeadm join 192.168.1.201:6443 --token 7qshlv.rscmjw3fjki5pq2a --discovery-token-ca-cert-hash sha256:3e3b9644ac29f53af2e3395bd200ce98f0610b74f063a35d5a13909171e779a7
```
## Installazione di CALICO sul MASTER
```
kubectl get no
```
```
NAME          STATUS     ROLES           AGE   VERSION
k8s-node-01   NotReady   control-plane   65s   v1.24.0
k8s-node-02   NotReady   <none>          16s   v1.24.0
```

Qui la [configurazione del file calico.yaml](calico.yaml)

Applicare il file yaml CALICO dal sito ufficiale:
```
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
```

```
kubectl get ns
```
```
NAME              STATUS   AGE
default           Active   67s
kube-node-lease   Active   69s
kube-public       Active   69s
kube-system       Active   69s
```
```
watch kubectl get po -n kube-system
```
```
NAME                                       READY   STATUS              RESTARTS   AGE
calico-kube-controllers-544df655bd-scg5n   0/1     Pending             0          14s
calico-node-8kngm                          0/1     Init:0/2            0          15s
calico-node-rrjkm                          0/1     Init:0/2            0          15s
coredns-6d4b75cb6d-vhhqn                   0/1     Pending             0          59s
coredns-6d4b75cb6d-wj2vj                   0/1     Pending             0          59s
etcd-k8s-node-03                           1/1     Running             0          72s
kube-apiserver-k8s-node-03                 1/1     Running             0          73s
kube-controller-manager-k8s-node-03        1/1     Running             0          72s
kube-proxy-tz69l                           0/1     ContainerCreating   0          27s
kube-proxy-v79fg                           1/1     Running             0          59s
kube-scheduler-k8s-node-03                 1/1     Running             0          74s
```

Attendere finchè i pod siano tutti su running come di seguito:
```
NAME                                       READY   STATUS    RESTARTS   AGE
calico-kube-controllers-544df655bd-scg5n   1/1     Running   0          4m25s
calico-node-8kngm                          1/1     Running   0          4m26s
calico-node-rrjkm                          1/1     Running   0          4m26s
coredns-6d4b75cb6d-vhhqn                   1/1     Running   0          5m10s
coredns-6d4b75cb6d-wj2vj                   1/1     Running   0          5m10s
etcd-k8s-node-03                           1/1     Running   0          5m23s
kube-apiserver-k8s-node-03                 1/1     Running   0          5m24s
kube-controller-manager-k8s-node-03        1/1     Running   0          5m23s
kube-proxy-tz69l                           1/1     Running   0          4m38s
kube-proxy-v79fg                           1/1     Running   0          5m10s
kube-scheduler-k8s-node-03                 1/1     Running   0          5m25s
```

## Installazione NGINX INGRESS sul MASTER

Qui la [configurazione del file nginx-ingress-controllers-deploy.yaml](nginx-ingress-controllers-deploy.yaml)

Applicare il file yaml di NGINX:
```
kubectl apply -f nginx-ingress-controllers-deploy.yaml
```

Inserito namespace ingress-nginx:
```
kubectl get ns
```
```
NAME              STATUS   AGE
default           Active   10m
ingress-nginx     Active   2m19s
kube-node-lease   Active   10m
kube-public       Active   10m
kube-system       Active   10m
```

I POD nel namspace ingress-nginx:
```
watch kubectl get po -n ingress-nginx
```
```
NAME                                       READY   STATUS      RESTARTS   AGE
ingress-nginx-admission-create-v2rqx       0/1     Completed   0          3m28s
ingress-nginx-admission-patch-66nnt        0/1     Completed   0          3m28s
ingress-nginx-controller-b66cc4b74-4w52g   1/1     Running     0          3m28s
```

## Test di Deployment pod
- Qui la [configurazione del file hotel1-deployment.yaml](hotel-test/hotel1-deployment.yaml)
- Qui la [configurazione del file hotel1-service.yaml](hotel-test/hotel1-service.yaml)
- Qui la [configurazione del file hotel2-deployment.yaml](hotel-test/hotel2-deployment.yaml)
- Qui la [configurazione del file hotel2-service.yaml](hotel-test/hotel2-service.yamlh)
- Qui la [configurazione del file hotel-ingress.yaml](hotel-test/hotel-ingress.yaml)

La cartella dove sono presenti i file è [hotel-test](hotel-test)

Applicare il deploy nel cluster:
```
kubectl apply -f hotel1-deployment.yaml
kubectl apply -f hotel1-service.yaml
kubectl apply -f hotel2-deployment.yaml
kubectl apply -f hotel2-service.yaml
kubectl apply -f hotel-ingress.yaml
```

Controllare i POD in stato running:
```
watch kubectl get po
```
```
NAME                      READY   STATUS    RESTARTS   AGE
hotel1-7fbb697c5b-7fjlr   1/1     Running   0          40s
hotel1-7fbb697c5b-r8vj7   1/1     Running   0          40s
hotel2-7678ff76b4-8sk6f   1/1     Running   0          39s
hotel2-7678ff76b4-gqnd8   1/1     Running   0          39s
Modificare il DNS o il file hosts con il parametro seguente:
```

Controllare lo stato dei service:
```
kubectl get svc
```
```
NAME         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
hotel1-svc   ClusterIP   10.102.96.132   <none>        80/TCP    2m14s
hotel2-svc   ClusterIP   10.99.197.97    <none>        80/TCP    2m13s
kubernetes   ClusterIP   10.96.0.1       <none>        443/TCP   19m
```

Controllare lo stato dell'ingress
```
kubectl get ing
```
```
NAME            CLASS   HOSTS       ADDRESS                       PORTS   AGE
hotel-ingress   nginx   hotel.luc   192.168.1.201,192.168.1.202   80      2m25s
```

Modificare il DNS oppure il file HOSTS:
```
192.168.1.201   hotel.luc
```

Dal browser digitare:
```
http://hotel.luc/h1
```

Risposta:
```
Server address: 10.0.127.197:80
Server name: hotel1-7fbb697c5b-r8vj7
Date: 14/May/2022:16:40:25 +0000
URI: /h1
Request ID: 077eb2bf16a9b85a9e18f04d1bc1e398
```

oppure
```
http://hotel.luc/h2
```

Risposta:
```
Server address: 10.0.127.199:80
Server name: hotel2-7678ff76b4-8sk6f
Date: 14/May/2022:16:40:28 +0000
URI: /h2
Request ID: 3a2890bdd3d5a26233797ca18ef159ac
```

## Troubleshooting
(Paragrafo da fare)


## Riferimenti
- https://www.ubuntu-it.org/download
- https://kubernetes.io/docs/setup/
- https://github.com/cri-o/cri-o/blob/main/install.md#readme
- https://citizix.com/how-to-install-cri-o-container-runtime-on-ubuntu-20-04/
- https://projectcalico.docs.tigera.io/about/about-calico
- https://kubernetes.github.io/ingress-nginx/deploy/
- https://iximiuz.com/en/posts/containerd-command-line-clients/
